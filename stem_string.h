/*
	Author: Jasmin Bakalovic
	Date: 08.10.2018

	Description: Since students haven't learned memory management and pointers,
	this allowes them to have a "string" type which is easier for them to grasp in the beginning.

	Done for stem-akademija.ba
*/
#ifndef _STEM_STRING
#define _STEM_STRING
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>

#define STEM_STRING_INIT_BUFFER 1024 
#define STEM_MAX_STRINGS 10

typedef struct string
{
	char* text;
	size_t size;
	size_t capacity;
} string;

static bool has_string = false;
static size_t strings_count = 0;

static char *strings[STEM_MAX_STRINGS];

static void clean_strings(void)
{
	for (size_t i = 0; i < strings_count; ++i)
	{
		free(strings[i]);
	}
}

static void change_old_location(char* str, char* new_str)
{
	for (size_t i = 0; i < strings_count; ++i)
	{
		if (strings[i] == str)
		{
			strings[i] = new_str;
			break;
		}
	}
}

string get_string(void)
{
	if (strings_count >= STEM_MAX_STRINGS)
	{
		string s = {NULL, 0, 0};
		return s;
	}

	char* str = (char *)malloc(sizeof(char) * STEM_STRING_INIT_BUFFER);
	if (str == NULL)
		exit(EXIT_FAILURE);

	size_t count = 0, capacity = STEM_STRING_INIT_BUFFER;

	int ch;
	while ((ch = getchar()) && (ch != EOF) && (ch != '\n'))
	{
		// Increase buffer to store more information
		// including null character
		if (count >= (capacity - 1))
		{
			capacity *= 2;
			
			char* new_str = (char *)malloc(sizeof(char) * capacity);
			if (new_str == NULL)
				exit(EXIT_FAILURE);

			if (memcpy(new_str, str, sizeof(char) * count) == NULL)
				exit(EXIT_FAILURE);

			// Because new allocation has happened, charachters are coppied, now str points to new location.
			// Have to chage in strings array pointer from old location to new
			change_old_location(str, new_str);

			free(str);
			str = new_str;	
		}

		str[count] = ch;
		++count;
	}

	str[count] = '\0';

	// Add string to strings array in order when clean_strings function is called
	// to free memory for next use. Better solution is to create dynamic list.
	strings[strings_count++] = str;

	// Call function clean_strings only if it has strig
	// in order to back memory to owner	
	if (count > 0 && has_string == false)
	{
		has_string = true;
		atexit(clean_strings);
	}

	string s = {str, count, capacity};
	return s;
}

// Compares equality of two strings
bool streq(const char* ls, const char* rs)
{
	if (strlen(ls) != strlen(rs))
		return false;

	return strcmp(ls, rs) == 0;	
}

// Concatenates string from s to member text of str  
void concat(string* str, const char* s)
{
	// If there is enough space to store characters, store it without any new allocation
	if ((str->capacity - str->size) >= (strlen(s) + 1))
	{
		strncpy(str->text + strlen(str->text), s, strlen(s));
		str->size = strlen(str->text);
		return;
	}

	// Double the capacity of the string member until characters from s could fit
	while ((str->capacity *= 2) && str->capacity < strlen(s))
		continue;

	char* new_str = (char *)malloc(sizeof(char) * str->capacity);
	if (new_str == NULL)
		exit(EXIT_FAILURE);

	// Copy characters from current text without null termination character
	strncpy(new_str, str->text, strlen(str->text) - 1);

	// Copy characters from s
	strncpy(new_str + strlen(str->text), s, strlen(s));

	// Change positions in strings array and free memory
	change_old_location(str->text, new_str);
	free(str->text);
	str->text = new_str;
}

#endif
